var https = require('https');
var url = require('url');
var slack_url = 'https://hooks.slack.com/services/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';
var slack_opts = url.parse(slack_url);
slack_opts.method = 'POST';
slack_opts.headers = {'Content-Type': 'application/json'};

exports.handler = function(event, context) {
	var user = event.Records[0].Sns.MessageAttributes.User.Value;
	var amount = event.Records[0].Sns.MessageAttributes.Level.Value;
	var payload = user + " just signed up and paid " + amount + ".";

	var req = https.request(slack_opts, function(res) {
		if (res.statusCode === 200) {
			context.succeed('OK');
		} else {
			context.fail('Status code: ' + res.statusCode);
		}
	});
	
	req.on('error', function(e) {
        console.log('problem with request: ' + e.message);
        context.fail(e.message);
    });
      
    req.write(JSON.stringify({text: JSON.stringify(payload, null, '  ')}));
      
    req.end();
}