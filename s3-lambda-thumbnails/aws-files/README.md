S3-Lambda-Thumbnails
===================

Create thumbnails from images uploaded to an S3 bucket.
-----------------------------------------------------------------------------

This directory contains the CloudFormation template, the Lambda function, and other files used to install dependencies and test the function locally.

## Dependencies and Installation

Ensure Node is installed. This can be done through your package manager or from their [website](http://nodejs.org/).

Make sure you are in the main directory (where this file is) and use `npm` to install the application's dependencies:

$ npm install

This will install dependencies defined in package.json

## Create the AWS environment

* The CloudFormation template needs to be changed to fit your needs.
First, change both "BucketName" properties to bucket names that don't already exist and that you have access to.
Second, change the "S3Bucket" to your own bucket where you are storing the Lambda function (s3-lambda-thumbnails.zip).

As long as your AWS account has the right permission, you can then run the template as-is and it will create the Lambda function, the 2 S3 buckets, and the event source mapping.

* Start the application and try it out!
Refer to the README in /s3-lambda-thumbnails/