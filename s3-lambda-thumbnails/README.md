NodeS3Uploader
===================

Upload images to S3 with this Node.js application
-----------------------------------------------------------------------------

This simple example uploads images directly to Amazon's S3. It is used to demonstrate how events can trigger Lambda functions, even when an application has no idea Lambda exists. The result is a back-end that can be plugged in and out of any application as long as the application uploads to the correct bucket.

This example uses the [express](http://expressjs.com/) web framework. However, the process of signing the S3 PUT request would be identical in most apps.

## Dependencies and Installation

Ensure Node is installed. This can be done through your package manager or from their [website](http://nodejs.org/).

Make sure you are in the main directory (where this file is) and use `npm` to install the application's dependencies:

$ npm install

This will install dependencies defined in package.json

## Running the application

* Create the AWS environment using the CloudFormation template in /aws-files/.
Please refer to the README in /aws-files/ for more info.

* Set environment variables for your AWS access key and secret key, as well as the bucket name you want to upload to.
These variables are set in the .env file

* Run the application with:
$ node app.js

(If you have a port conflict, you can set PORT in your .env. The default port used is 3000)

* Shut down the application with Ctrl + C